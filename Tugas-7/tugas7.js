// Soal 1
console.log("----No.1----")
// Realease 0
console.log("----Realease 0----")
class Animal {
    constructor(name){
      this.name = name;
      this.legs = 4;
      this.cold_bloodded = false;
    }
    }
    var sheep = new Animal("shaun");
    
    console.log(sheep.name) // "shaun"
    console.log(sheep.legs) // 4
    console.log(sheep.cold_bloodded) // false
    console.log()  
  // Realease 1
  console.log("----Realease 1----")
  class Ape extends Animal {
    constructor(name){
        super(name)
        this.legs = 2;
    }
  
    yell(){
        return "Auooo";
    }
  }
  
  class Frog extends Animal {
    constructor(name){
        super(name)
    }
    jump(){
        return "hop hop";
    }
  }
  
  var sungokong = new Ape("kera sakti");
  console.log(sungokong.yell()); // "Auooo"
  
  var kodok = new Frog("buduk");
  console.log(kodok.jump()); // "hop hop" 
  console.log()  
  
  // Soal 2
  console.log("----No.2----")
class Clock {
  constructor({ template }){
        this.template = template;
        this.timer;
  }
    render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;

      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

      console.log(output);
    }
    stop (){
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
  }


var clock = new Clock({template: 'h:m:s'});
clock.start();  
