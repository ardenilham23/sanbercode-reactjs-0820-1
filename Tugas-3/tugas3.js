console.log()
//No.1
console.log("----No.1----")
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var upper = kataKeempat.toUpperCase();
function titleCase(str) {
    return str.toLowerCase().split(' ').map(function(word) {
      return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
  }
titleCase("senang");
console.log(kataPertama+" "+titleCase("senang")+" "+kataKetiga+" "+upper)

console.log()
//No.2
console.log("----No.2----")
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
var number1 = Number(kataPertama);
var number2 = Number(kataKedua);
var number3 = Number(kataKetiga);
var number4 = Number(kataKeempat);
console.log(number1+number2+number3+number4);

console.log()
//No.3
console.log("----No.3----")
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0,3); 
var kataKedua = kalimat.substring(4,14);  
var kataKetiga= kalimat.substring(15,18);  
var kataKeempat = kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25,); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

console.log()
//No.4
console.log("----No.4----")
var nilai = 75;

if ( nilai >= 80 ) {
    console.log("A")
} else if ( nilai >= 70 ) {
    console.log("B")
} else if ( nilai >= 60 ) {
    console.log("C")
} else if ( nilai >= 50 ) {
    console.log("D")
} else {
    console.log("E")
}


console.log()
//No.5
console.log("----No.5----")
var tanggal = 22;
var bulan = 5;
var tahun = 1995;

var strBulan

switch(bulan){
  case 1: {
    strBulan = "Januari";
    break;
  }
  case 2: {
    strBulan = "Februari";
    break;
  }
  case 3: {
    strBulan = "Maret";
    break;
  }
  case 4: {
    strBulan = "April";
    break;
  }
  case 5: {
    strBulan = "Mei";
    break;
  }
  case 6: {
    strBulan = "Juni";
    break;
  }
  case 7: {
    strBulan = "Juli";
    break;
  }
  case 8: {
    strBulan = "Agustus";
    break;
  }
  case 9: {
    strBulan = "September";
    break;
  }
  case 10: {
    strBulan = "Oktober";
    break;
  }
  case 11: {
    strBulan = "November";
    break;
  }
  case 12: {
    strBulan = "Desember";
    break;
  }
  default:{
    strBulan = "invalid"
  }
}

console.log(tanggal + " " + strBulan + " " + tahun)
console.log()
