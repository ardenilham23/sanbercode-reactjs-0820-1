//No.1
console.log("----No.1----")
function halo(){
    return "Halo Sanbers!";
}
console.log(halo());

console.log()
//No.2
console.log("----No.2----")
function kalikan(a, b){
    return a*b;
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)


console.log()
//No.3
console.log("----No.3----")
function introduce(a, b,c,d){
    return "nama saya " + a + ", umur saya "+ b+" tahun, alamat saya di "+c+", dan saya punya hobby yaitu "+d+"!";
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

console.log()


//No.4
console.log("----No.4----")
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var arrayDaftarPesertaObj = {
    nama : arrayDaftarPeserta[0],
    gender : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahun_lahir : arrayDaftarPeserta[3]
}
console.log(arrayDaftarPesertaObj);


console.log()
//No.5
console.log("----No.5----")
var arrbuah = [{nama: "strawberry" , warna: "merah" , adabijinya: "tidak ada" , harga: 9000},
{nama: "jeruk" , warna: "oranye" , adabijinya: "ada" , harga: 8000},
{nama: "Semangka" , warna: "Hijau & Merah" , adabijinya: "ada" , harga: 10000},
{nama: "Pisang" , warna: "Kuning" , adabijinya: "ada" , harga: 5000}];

console.log(arrbuah[0]);

console.log()
//No.6
console.log("----No.6----")

var dataFilm = []

function tambahDataFilm(nama, durasi, genre, tahun){
  dataFilm.push({
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun
  })
}

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

console.log(dataFilm)
console.log()
