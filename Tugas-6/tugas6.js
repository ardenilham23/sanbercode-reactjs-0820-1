//No.1
console.log("----No.1----")


let luasLingkaran = (r) => {
    const pi = Math.PI
    let hasil = pi*(r*r);
    return hasil;
}
let kelilingLingkaran = (r) => {
    const pi = Math.PI
    let hasil = pi*r*2;
    return hasil;
}
console.log("Luas lingkaran = "+luasLingkaran(7));
console.log("Keliling lingkaran = "+kelilingLingkaran(7));

console.log()

//No.2
console.log("----No.2----")

let kalimat = ""

let kalimatku = (a) => {
    return kalimat += `${a} `
}
kalimatku("saya")
kalimatku("adalah")
kalimatku("seorang")
kalimatku("frontend")
kalimatku("developer")
console.log(kalimat);

console.log()

//No.3
console.log("----No.3----")
  const newFunction = (firstName, lastName) => {
    return {
        firstName, lastName, fullName: function() {
            console.log(firstName+" "+lastName);
            }
        }
    }

  newFunction("William", "Imoh").fullName() 
 
  console.log()
//No.4
console.log("----No.4----")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const {firstName, lastName, destination, occupation, spell} = newObject

  console.log(firstName, lastName, destination, occupation)
  console.log()

  //No.5
console.log("----No.5----")
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  const combined = [...west, ...east]
  console.log(combined)