//No.1
console.log("----No.1----")
var angka1 =2;
var angka2 =2;
console.log("LOOPING PERTAMA")
while(angka1 <=20){
  console.log(angka1 + " - I love coding")
  angka1 = angka1+angka2
}

var angka3 = 20;
console.log("LOOPING KEDUA")
while(angka3 > 0){
  console.log(angka3 + " - I will become a frontend developer")
  angka3 = angka3-angka2
}

console.log()
//No.2
console.log("----No.2----")
var angka;

for (angka =1; angka <= 20; angka++){
  if  (angka % 3 === 0 && angka % 2 === 1){
    console.log(angka + " - I Love Coding")
  } else if (angka % 2 === 0){
    console.log(angka + " - Berkualitas")
  } else if (angka % 2 === 1){
    console.log(angka + " - Santai")
  }
}
console.log()
//No.3
console.log("----No.3----")

for(var i = 1; i <= 7; i++) {
  var x = "";
  for(var j = 1; j <= i; j++) {
   x = x + '#';
  }
  console.log(x);
}
console.log()


//No.4
console.log("----No.4----")

var kalimat="saya sangat senang belajar javascript"
 
var name = kalimat.split(" ")
console.log(name);

console.log()
//No.5
console.log("----No.5----")

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortDaftarBuah = daftarBuah.sort()
for(var i = 0; i <= 4; i++) {
  console.log(sortDaftarBuah[i])
}
